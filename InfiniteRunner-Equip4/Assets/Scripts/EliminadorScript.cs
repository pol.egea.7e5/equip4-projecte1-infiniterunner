using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EliminadorScript : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(collision.collider.gameObject);
        if (collision.collider.tag=="Player")
        {
            PlayerPrefs.SetString("score", GameManager.Instance.actualscore.ToString());

            SceneManager.LoadScene("FinalScene");
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
    }
}
