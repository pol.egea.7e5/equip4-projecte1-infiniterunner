using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerarPlat : MonoBehaviour
{

    // colocar el punto inicial de una plataforma en el mismo sitio que el punto final de la anterior
    //Esto para hacer mas desde el unity


    [SerializeField]
    private GameObject[] partesNivel;

    [SerializeField]
    private float distmin; // distancia a la que detecta al jugador para spawnear la siguente prefab

    [SerializeField]
    private Transform puntofinal;// punto que se debe colocar el punto donde acaba el mapa

    [SerializeField]
    private int cantidadplataformas;// cantidad de plataformas que spawnea antes de verla  (Sirve para que las plataformas no spawnee dentro de la camara)

    private Transform jugador;
    public GameObject boss;
    private int contPlatforms;


    void Start()
    {
        jugador = GameObject.FindGameObjectWithTag("Player").transform;
        for (int i = 0; i < cantidadplataformas; i++)
        {
            GenerarPlataformas();
        }
    }

    // Update is called once per frame
    void Update()
    {

        //if (boss.GetComponent<Boss>().die) ActivarBoss();
        if(jugador.gameObject!=null)if (Vector2.Distance(jugador.position, puntofinal.position) < distmin)
        {
            contPlatforms++;

            GenerarPlataformas();
            Debug.Log(contPlatforms+"Platadormas");

                if (contPlatforms == GameManager.Instance.SpawnBoss)
                {
                    ActivarBoss();
                    contPlatforms = 0;
                }
           
        }
        
    }
    void GenerarPlataformas()
    {
        int RandomNum = Random.Range(0, partesNivel.Length);
        GameObject nivel = Instantiate(partesNivel[RandomNum], puntofinal.position, Quaternion.identity);
        puntofinal = BuscarpuntoFinal(nivel, "PuntoFinal");
    }

    private Transform BuscarpuntoFinal(GameObject parteNivel, string etiqueta) // detecta cuando el jugador pasa por el punto final
    {
        Transform punto = null;
        foreach (Transform ubicacion in parteNivel.transform)
        {
            if (ubicacion.CompareTag(etiqueta))
            {
                punto = ubicacion;
                break;
            }
        }

        return punto;
    }
    public void ActivarBoss()
    {

                boss.GetComponent<Boss>().hp = 100;
                boss.GetComponent<Boss>().estado = 0;
                boss.GetComponent<Boss>().die = false;
                boss.SetActive(true);
 
    }
}
