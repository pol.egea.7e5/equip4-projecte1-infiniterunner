using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Wilberforce;

public class VisionPlat : MonoBehaviour
{
    // Start is called before the first frame update
    public int type;
    private TilemapRenderer tilemaprenderer;
    void Start()
    {
        type = Random.Range(0, 4);
        tilemaprenderer = GetComponent<TilemapRenderer>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (type != 0&& tilemaprenderer!=null && GameManager.Instance.filtreDaltonisme!=null) { 
            if ((type == GameManager.Instance.filtreDaltonisme.Type || GameManager.Instance.filtreDaltonisme.Type == 0) && tilemaprenderer.enabled==false) tilemaprenderer.enabled = true;
            else if (type != GameManager.Instance.filtreDaltonisme.Type&& GameManager.Instance.filtreDaltonisme.Type!=0 && tilemaprenderer.enabled==true) tilemaprenderer.enabled = false;
        }

    }
}
