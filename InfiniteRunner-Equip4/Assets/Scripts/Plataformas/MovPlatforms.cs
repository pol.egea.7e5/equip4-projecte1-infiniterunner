using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovPlatforms : MonoBehaviour
{
    public float speed;


    void Start()
    {
        speed = GameManager.Instance.speedplatafromas;
        if (speed==0)
        {
            speed = GameObject.Find("GameManager").GetComponent<GameManager>().speedplatafromas;
        }
    }


    void Update()
    {
        ManageMov();
        EliminarPlat();
    }
    public void EliminarPlat()
    {
        if (transform.position.x <= -60)
        {
            Destroy(gameObject);
        }
        
    }

    void ManageMov()
    {
        GetComponent<Rigidbody2D>().velocity =-GameManager.Instance.speedplatafromas*Vector2.right;
        GetComponent<Rigidbody2D>().gravityScale = 1;
        //this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x - speed * Time.deltaTime, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
    }
}
