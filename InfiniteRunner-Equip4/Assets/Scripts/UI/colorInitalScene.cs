using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using Wilberforce;

public class colorInitalScene : MonoBehaviour
{
    float time;
    float basetime;
    public Colorblind filtreDaltonisme;
    // Start is called before the first frame update
    void Start()
    {
        filtreDaltonisme = GetComponent<Colorblind>();
        basetime = 7;
        time = basetime;
    }

    // Update is called once per frame
    void Update()
    {
        CheckDaltonism();   
    }


    public void CheckDaltonism()
    {
        if (time <= 0)
        {
            if (filtreDaltonisme.Type == 3) filtreDaltonisme.Type = 0;
            else filtreDaltonisme.Type += 1;
            time = basetime;
        }
        else time -= Time.deltaTime;
    }
}
