using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToGame : MonoBehaviour
{
    public void gotoGame()
    {
        SceneManager.LoadScene("GameScene");
    }
    public void goToStart()
    {
        SceneManager.LoadScene("StartScene");
    }
    public void exitApp()
    {
        Application.Quit();
    }
}
