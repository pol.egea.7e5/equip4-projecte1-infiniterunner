using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Controler : MonoBehaviour
{
    public MC mcData;
    private float _health;
    private float _totalHealth;
    private float _money;

    // Start is called before the first frame update
    void Start()
    {
        _totalHealth = mcData.MC_MaxHealth;
    }
    private void OnEnable()
    {
        MC.changeLife += ChangeLife;
        MC.updateMoney += UpdateMoney;
    }
    private void OnDisable()
    {
        MC.updateMoney -= UpdateMoney;
        MC.changeLife -= ChangeLife;
    }

    public void UpdateMoney()
    {
        _money = mcData.GetMC_Moneythisgame();
        GetComponentInChildren<Text>().text = $"{_money}";
    }
    public void ChangeLife()
    {
        _health = mcData.GetMC_LeftHealth();
        if (_health >= 0) GetComponentInChildren<Scrollbar>().size = _health / _totalHealth;
        else GetComponentInChildren<Scrollbar>().size = 0;
        Debug.Log(GetComponentInChildren<Scrollbar>().size);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
