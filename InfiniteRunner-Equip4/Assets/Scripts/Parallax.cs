using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField] private Vector2 velMov;
    private Vector2 offset;
    private Material material;



    private void Awake()
    {
        material = GetComponent<SpriteRenderer>().material;
  
    }

    // Update is called once per frame
    void Update()
    {
        offset = velMov * Time.deltaTime;
        material.mainTextureOffset += offset;
    }
}
