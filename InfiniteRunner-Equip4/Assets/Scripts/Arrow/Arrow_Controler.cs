using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow_Controler : MonoBehaviour
{
    [SerializeField] private Arrow_Data AD;
    // Start is called before the first frame update
    private Rigidbody2D rigidbody2D;
    private Vector3 mouse;
    private Camera mainCamera;
    public float damage;

    void Start()
    {
        damage = AD.damage;
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        mouse = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        rigidbody2D = GetComponent<Rigidbody2D>();
        Vector3 direction = mouse - transform.position;
        rigidbody2D.velocity = new Vector2(direction.x, direction.y).normalized * AD.speed;

    }
    void Update()
    {
        float angle = Mathf.Atan2(rigidbody2D.velocity.y,rigidbody2D.velocity.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Eliminador")
        {
            Destroy(this.gameObject);
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="Minion")
        {

            Destroy(this.gameObject);
        }
    }
}
