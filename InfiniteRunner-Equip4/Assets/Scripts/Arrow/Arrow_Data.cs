using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Arrow_Data", menuName = "New Arrow")]
public class Arrow_Data : ScriptableObject
{
    public float damage;
    public float speed;
}
