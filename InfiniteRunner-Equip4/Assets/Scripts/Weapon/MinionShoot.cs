using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionShoot : Weapon
{
    public MC mcData;
    public float timeRangeShoot;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (timeRangeShoot <= 0) Destroy(gameObject);
        rb.velocity = (speed+4) * new Vector2(-1,0);
        timeRangeShoot -= Time.deltaTime;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") mcData.SetMC_LeftHealth(mcData.GetMC_LeftHealth()-damage);
        if (collision.tag != "Minion") Destroy(gameObject);
    }
}
