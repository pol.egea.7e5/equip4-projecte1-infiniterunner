using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Wilberforce;

public enum DaltoTipus
{
    Normal,Protanopia,Deuteranopia,Tritanopia
}
public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public Text score;
    public int actualscore;
    public float speedplatafromas = 2;
    public float timecounter;
    public float targetScoreTime;
    public int SpawnBoss; //El numero es segun el numero de plataformas spawneadas
    public Colorblind filtreDaltonisme;
    public GameObject Main;

    private float speedmax = 10f;
    public static GameManager Instance
    {
        get { return _instance; }
    }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    public void Sumarvelocidad()
    {
        if (speedplatafromas< speedmax)
        {
            speedplatafromas += 0.06f * Time.deltaTime;
        }
        
    }
    void Start()
    {
        Main = GameObject.FindGameObjectWithTag("Player");
        score = GameObject.Find("Score").GetComponent<Text>();
        filtreDaltonisme = GameObject.Find("Main Camera").GetComponent<Colorblind>();
        targetScoreTime = 0.1f;
        timecounter = 0;
        score.text = "0";
        actualscore = 0;
        int randomRespawn = Random.Range(0, 10);
        SpawnBoss = randomRespawn;
    }
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "GameScene") 
        {
            Sumarvelocidad();
        }

           
    }
    private void FixedUpdate()
    {
        if (score == null && SceneManager.GetActiveScene().name == "GameScene")
        {
            score = GameObject.Find("Score").GetComponent<Text>();
            Main = GameObject.FindGameObjectWithTag("Player");
            filtreDaltonisme = GameObject.Find("Main Camera").GetComponent<Colorblind>();
            score.text = "0";
            actualscore = 0;
            timecounter = 0;
            speedplatafromas = 2;
        }
        if (SceneManager.GetActiveScene().name == "GameScene")
            actualscore += 1;
        {
            if (timecounter <= targetScoreTime) timecounter += Time.deltaTime;
            else
            {
                timecounter = 0;
                score.text = actualscore.ToString();
                CheckDaltonism();
            }
        }
        

    }
    public void CheckDaltonism()
    {
        if (actualscore != 0 && actualscore % 30 == 0) {
            if (filtreDaltonisme.Type==3)filtreDaltonisme.Type=0;
            else filtreDaltonisme.Type += 1;
        }
    }

}
