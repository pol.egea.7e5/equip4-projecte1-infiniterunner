using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public float hp;
    public float moveSpeed;
    [SerializeField]
    protected float jumpSpeed;
    protected Vector2 shootDirection;
    public float damageMelee;
}
