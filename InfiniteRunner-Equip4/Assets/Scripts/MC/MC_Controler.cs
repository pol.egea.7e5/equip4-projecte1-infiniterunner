using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;

public class MC_Controler : Character
{
    [SerializeField] public MC mcData;
    protected Rigidbody2D rigidBody2D;
    private int money;
    public Animator animation;
    [SerializeField] private GameObject arrow;
    protected enum jumpstate
    {

        floor,
        doublejump,
        rising,
        fall

    };
    protected jumpstate jumpState;
    void Start()
    {
        animation = GetComponent<Animator>();
        mcData.SetMC_LeftHealth(mcData.MC_MaxHealth);
        hp = mcData.MC_MaxHealth;
        moveSpeed = mcData.MC_MoveSpeed;
        jumpSpeed = mcData.MC_JumpSpeed;
        rigidBody2D = GetComponent<Rigidbody2D>();
        jumpState = jumpstate.floor;
        money = mcData.GetMC_Moneythisgame();
    }

    // Update is called once per frame
    void Update()
    {

        
      
        if (Input.GetKeyDown("m"))
        {
            Debug.Log("m pressed");
            mcData.SetMC_LeftHealth(mcData.GetMC_LeftHealth() - 5);
            mcData.SetMC_Moneythisgame(mcData.GetMC_Moneythisgame() + 1 );

        }
        walk();
        jump();

        if (mcData.MC_reloadTimeLeft <= 0) { if (Input.GetMouseButtonDown(0)) shoot(); }

        else mcData.MC_reloadTimeLeft = mcData.MC_reloadTimeLeft - Time.deltaTime;


        
    }
    void walk()
    {
        float moving = moveSpeed * Input.GetAxis("Horizontal");
        if (jumpState == jumpstate.floor) { animation.SetFloat("walkState", moving); }
        else {
            moving = (moveSpeed * Input.GetAxisRaw("Horizontal")) / 1.3f ;
            animation.SetFloat("walkState", moving); 
        }
        rigidBody2D.velocity = new Vector2(moving, rigidBody2D.velocity.y);
    }

    void jump()
    {
        if (Input.GetKeyDown(KeyCode.W) && jumpState == jumpstate.floor)
        {
            animation.SetFloat("jumpState", 1);
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1) * jumpSpeed, ForceMode2D.Impulse);
            jumpState = jumpstate.fall;

        }

    }
    void shoot()
    {

        Vector2 pos = new Vector2(transform.position.x, transform.position.y);
        GameObject objectToSpawn = arrow;
        Instantiate(objectToSpawn, pos, transform.rotation);
        mcData.MC_reloadTimeLeft = mcData.MC_reloadTime;
    }

    protected void checkdeath()
    {
        if (mcData.GetMC_LeftHealth() <= 0)
        {
            PlayerPrefs.SetString("score", GameManager.Instance.actualscore.ToString());
            SceneManager.LoadScene("FinalScene");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Plataformas" || collision.collider.tag == "Ground")
        {
            animation.SetFloat("jumpState", 0);
            //Debug.Log("Changed");
            jumpState = jumpstate.floor;
        }

        if (collision.collider.tag == "Minion" && collision.collider.transform.parent.gameObject.GetComponent<Minions>().tipus == MinionType.Melee)
        {
            
            mcData.SetMC_LeftHealth(mcData.GetMC_LeftHealth() - collision.collider.transform.parent.gameObject.GetComponent<Minions>().damageMelee);
            checkdeath();
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Eliminador")
        {
            SceneManager.LoadScene("FinalScene");
        }
        if (collision.tag == "EvilWeapon")
        {
            checkdeath();
        }
    }
}
