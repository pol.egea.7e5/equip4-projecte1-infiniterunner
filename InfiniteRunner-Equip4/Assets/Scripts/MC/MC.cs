using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "MC_Data", menuName = "New MC")]
public class MC : ScriptableObject
{
    public static event Action changeLife = delegate { };
    public static event Action updateMoney = delegate { };
    public float MC_MaxHealth;
 
    [SerializeField]  private float MC_LeftHealth;
    [SerializeField] private int MC_Moneythisgame;
    public int MC_MoneyTotal;
    public float MC_MoveSpeed;
    public float MC_JumpSpeed;
    public float MC_reloadTime;
    public float MC_reloadTimeLeft;
    public void SetMC_LeftHealth(float h)
    {
        MC_LeftHealth = h;
        changeLife.Invoke();
    }
    public float GetMC_LeftHealth()
    {
        return MC_LeftHealth;
    }

    public void SetMC_Moneythisgame(int h)
    {
        MC_LeftHealth = h;
        updateMoney.Invoke();
    }
    public int GetMC_Moneythisgame()
    {
        return MC_Moneythisgame;
    }
}