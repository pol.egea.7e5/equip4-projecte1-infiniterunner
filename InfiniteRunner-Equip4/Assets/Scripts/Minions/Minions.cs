using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum MinionType
{
    Melee,Shooter
}
public class Minions : Character
{
    public MinionType tipus;
    MinionsFuncts _funcs;
    public Object bala;
    public GameObject platform;
    public Animator animator;
    public bool die = false;
    private void Start()
    {
        platform = GameObject.FindGameObjectWithTag("Plataformas");
        _funcs = ScriptableObject.CreateInstance<MinionsFuncts>();
        animator = gameObject.GetComponent<Animator>();
        _funcs.hpAct = hp;
        _funcs.minion = gameObject.GetComponent<Minions>();
        GetComponent<Rigidbody2D>().velocity = new Vector2(-GameManager.Instance.speedplatafromas, GetComponent<Rigidbody2D>().velocity.y) + moveSpeed * new Vector2(-1, 0);
        switch (tipus)
        {
            case MinionType.Melee:
                animator.Play("Minion_Arbre_Run");
                break;
            case MinionType.Shooter:
                animator.Play("Minion_Planta_Idle");
                break;
        }
    }
    private void Update()
    {

        if(!die)switch (tipus)
        {
            case MinionType.Melee:
                _funcs.Movement();
                break;
            case MinionType.Shooter:
                _funcs.timeToShootCont += Time.deltaTime;
                _funcs.ShootAtack(bala);
                break;
        }
        checkdeath();
    }



    private void checkdeath()
    {
        var disable = _funcs.CheckDie();
        if (disable && tipus == MinionType.Shooter)
        {
            foreach (Collider2D col in GetComponents<Collider2D>())
            {
                col.enabled = false;
            }
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Plataformas"&&platform==null) platform = collision.collider.gameObject;
        if (tipus == MinionType.Melee)
        {
            if (collision.collider.tag == "Player") _funcs.MeleeAttackAnim();
            if (collision.collider.tag == "Minion"|| collision.collider.tag == "Player")
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x*-1, GetComponent<Rigidbody2D>().velocity.y);
                GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
            }
        }
        if(collision.collider.tag=="Weapon")_funcs.GetMeleeAtack(collision.collider.GetComponent<Weapon>().damage);
        
        if (collision.collider.tag == "Eliminador")
        {
            Destroy(this.gameObject);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Arrow")
        {
            _funcs.hpAct = _funcs.hpAct - collision.gameObject.GetComponent<Arrow_Controler>().damage;

        }
        else if (collision.tag == "Eliminador")
        {
            Destroy(this.gameObject);
        }
    }
}
