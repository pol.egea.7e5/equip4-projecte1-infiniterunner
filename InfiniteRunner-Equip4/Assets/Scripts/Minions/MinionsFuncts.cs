using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionsFuncts : ScriptableObject
{
    public Minions minion;
    public float hpAct;
    public float timeToShootCont=0f;
    public float timeToShoot=0.75f;
    public void GetMeleeAtack(float damage)
    {
        hpAct -= damage;
    }
    public void ShootAtack(Object bala)
    {
        if (timeToShootCont >= timeToShoot)
        {
            minion.animator.Play("Minion_Planta_Attack");
            Instantiate(bala, minion.transform.GetChild(2).transform.position, new Quaternion(0, 0, 0, 0));
            timeToShootCont = 0;
        }
    }
    public bool CheckDie()
    {
        if (hpAct <= 0) {
           /* foreach (Collider2D collider in minion.GetComponentsInChildren<Collider2D>())
            {
                //collider.;
            }*/
            if(minion.tipus!=MinionType.Melee)minion.animator?.Play("Minion_Planta_Die");
            else minion.animator?.Play("Minion_Arbre_Die");
            minion.die = true;
            
        }
       return minion.die;
    }
    public void MeleeAttackAnim()
    {
        minion.animator?.Play("Minion_Arbre_Attack");
    }
    public void Movement()
    {
        Vector3 originLeft = minion.transform.position - new Vector3(1,0.5f,0);
        Vector3 originRight = minion.transform.position - new Vector3(-1, 0.5f, 0);
        Rigidbody2D rb = minion.GetComponent<Rigidbody2D>();
        SpriteRenderer sp = minion.GetComponent<SpriteRenderer>();
        var hit = Physics2D.Raycast(originRight, Vector3.down, 2f);
        Debug.DrawRay(originRight, Vector3.down * 2, Color.green);
        if (hit.collider == null || (hit.collider != null && hit.collider.tag != "Plataformas"))
        {
           // Debug.Log("border1");
            sp.flipX = true;
            rb.velocity = new Vector2(-GameManager.Instance.speedplatafromas, rb.velocity.y) +minion.moveSpeed * new Vector2(-1,0);

        }
        else {
            hit = Physics2D.Raycast(originLeft, Vector3.down, 2f);
            Debug.DrawRay(originLeft, Vector3.down * 2, Color.red);
            if (hit.collider == null || (hit.collider != null && hit.collider.tag != "Plataformas"))
            {
               // Debug.Log("border2");
                sp.flipX = false;
                rb.velocity = new Vector2(-GameManager.Instance.speedplatafromas, rb.velocity.y) + minion.moveSpeed * new Vector2(1, 0);

            }
            else
            {
                if (sp.flipX) rb.velocity=new Vector2(-GameManager.Instance.speedplatafromas, rb.velocity.y) + minion.moveSpeed * new Vector2(-1, 0); 
                else rb.velocity = new Vector2(-GameManager.Instance.speedplatafromas, rb.velocity.y) + minion.moveSpeed * new Vector2(1, 0);
            }
        } 
    }
 
}
