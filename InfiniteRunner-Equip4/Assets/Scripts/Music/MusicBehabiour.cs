using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicBehabiour : MonoBehaviour
{
    public List<AudioClip> audioClips;
    [SerializeField]private AudioSource ASB;
    [SerializeField]private AudioSource ASVFX;
    [SerializeField]private AudioSource BGM;
    private float time;
    private enum bossState
    {
        no,
        yes
    }
    private bossState bS;
    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.Find("Boss")) bS = bossState.no;
        else bS = bossState.yes;
        time = 1;
     
    }

    // Update is called once per frame
    void Update()
    {
        if (time <= 0 && bS == bossState.yes)
        {
            ASVFX.Play();
            time = 10;
        }
        else time -= Time.deltaTime;
        Backgroundmusic();


    }
    private  void Backgroundmusic()
    {
        if (GameObject.Find("Boss") && bS == bossState.no)
        {
            //https://johnleonardfrench.com/how-to-fade-audio-in-unity-i-tested-every-method-this-ones-the-best/#first_method
            //FadeAudioSource.StartFade(BGM, 0.5f, 1);



            Fade(audioClips[1],1);
          //  BGM.PlayOneShot(audioClips[1]) ;
            bS = bossState.yes;
        }
        else if (!GameObject.Find("Boss") && bS == bossState.yes)
        {

            // FadeAudioSource.StartFade(BGM, 0.5f, 1);

            Fade(audioClips[0], 1);
            // BGM.PlayOneShot(audioClips[0]);
            bS = bossState.no;
        }
    }

   


    public void Fade(AudioClip clip, float volume)
    {
        StartCoroutine(FadeIt(clip, volume));
    }
    IEnumerator FadeIt(AudioClip clip, float volume)
    {

        ///Add new audiosource and set it to all parameters of original audiosource
        AudioSource fadeOutSource = gameObject.AddComponent<AudioSource>();
        fadeOutSource.clip = GetComponent<AudioSource>().clip;
        fadeOutSource.time = GetComponent<AudioSource>().time;
        fadeOutSource.volume = GetComponent<AudioSource>().volume;
        fadeOutSource.outputAudioMixerGroup = GetComponent<AudioSource>().outputAudioMixerGroup;

        //make it start playing
        fadeOutSource.Play();

        //set original audiosource volume and clip
        GetComponent<AudioSource>().volume = 0f;
        GetComponent<AudioSource>().clip = clip;
        float t = 0;
        float v = fadeOutSource.volume;
        GetComponent<AudioSource>().Play();

        //begin fading in original audiosource with new clip as we fade out new audiosource with old clip
        while (t < 0.98f)
        {
            t = Mathf.Lerp(t, 1f, Time.deltaTime * 0.2f);
            fadeOutSource.volume = Mathf.Lerp(v, 0f, t);
            GetComponent<AudioSource>().volume = Mathf.Lerp(0f, volume, t);
            yield return null;
        }
        GetComponent<AudioSource>().volume = volume;
        //destroy the fading audiosource
        Destroy(fadeOutSource);
        yield break;
    }


}
