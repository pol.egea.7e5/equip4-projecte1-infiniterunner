using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBullet : Weapon
{
    public MC mcData;
    public float timeRangeShoot;
    MC_Controler target;
    Vector2 movedirec;
    Boss boss;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        target = GameObject.FindObjectOfType<MC_Controler>();
        movedirec = (target.transform.position - transform.position).normalized * speed;
        rb.velocity = new Vector2(movedirec.x, movedirec.y);
        BulletRotation();

    }
    void Update()
    {
    }
    public void BulletRotation()
    {
        Vector3 direction =target.transform.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        rb.rotation = angle+180;
      
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        int daltonisbala = Random.Range(0,5);
        if (collision.tag == "Player")
        {
            mcData.SetMC_LeftHealth(mcData.GetMC_LeftHealth() - damage);
            Destroy(this.gameObject);
        }
        if (collision.tag == "Plataformas")
        {
            Physics2D.IgnoreCollision(collision, GetComponent<Collider2D>(), true);
        }
        if (collision.tag == "Minion" || collision.tag == "Ground") Destroy(this.gameObject);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

       
    }
}
