using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFunc : ScriptableObject
{
    public Boss boss;
    public float vida = 100f;
    public float timeToShootCont = 0f;
    
    public float spawnradio = 2f;
    public float spawntime = 3f;
    public float tiempoSiguienteEnem;
    //public bool accion = false;



    public void ShootAtack(Object bala, Transform zonadisp)
    {
        if (timeToShootCont >= boss.timeToShoot)
        {
            Instantiate(bala, zonadisp.position, new Quaternion(0, 0, 0, 0));
            timeToShootCont = 0;
            
        }
    }

    public void SpawnEnemie(Animator animation,float minX, float maxX, float minY, float maxY)
    {
        
        tiempoSiguienteEnem += Time.deltaTime;

        if (tiempoSiguienteEnem >= boss.spawnEnemigo)
        {
            
            tiempoSiguienteEnem = 0;
            Crearenemies(minX, maxX, minY, maxY);
            animation.SetTrigger("TriggerSpawn");
        }
    }
    private void Crearenemies(float minX, float maxX, float minY, float maxY)
    {
        int numEnemies = Random.Range(0,boss.enemigos.Length);
        Vector2 posicionAlea = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));

        Instantiate(boss.enemigos[numEnemies], posicionAlea, new Quaternion(0,180,0,0));
    }

     

}
