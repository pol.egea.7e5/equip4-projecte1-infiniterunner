using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Boss : Character
{

    public enum EstadoVida
    {
        Verde, Amarillo, Rojo  //Estado de vida como pokemon   (Full vida = verde)
    }
    [SerializeField]
    public EstadoVida estado;
    public Object[] bala;
    //public int probabilidaddestroybullet;
    public Transform zonadisp;
    BossFunc _funcBoss;
    public GameObject[] enemigos;
    private float minX, maxX, minY, maxY;
    [SerializeField] private Transform[] zonarespawn;

    public float spawnEnemigo;
    public float timeToShoot = 0.75f;
    private Animator animation;
    public int diescore;

    public bool die = false;



    
    private void Start()
    {
        animation = GetComponent<Animator>();
        maxX = zonarespawn.Max(zonarespawn => zonarespawn.position.x);
        minX = zonarespawn.Min(zonarespawn => zonarespawn.position.x);
        maxY = zonarespawn.Max(zonarespawn => zonarespawn.position.y);
        minY = zonarespawn.Min(zonarespawn => zonarespawn.position.y);

      

        _funcBoss = ScriptableObject.CreateInstance<BossFunc>();
        _funcBoss.vida = hp;
        _funcBoss.boss = gameObject.GetComponent<Boss>();

    }
    void Update()
    {

        //RecibirDamage();
        if(!die)EstadosCombate();
    }
    public void EstadosCombate()
    {
        int balarandom = Random.Range(0, bala.Length);
        switch (estado)
        {
            case EstadoVida.Verde:
                _funcBoss.timeToShootCont += Time.deltaTime;
                _funcBoss.ShootAtack(bala[balarandom], zonadisp);
                animation.SetBool("Attack2", true);
                animation.SetBool("Attack1", false);
                break;
            case EstadoVida.Amarillo:
                //  animation.SetTrigger("TriggerSpawn");
                animation.SetBool("Attack2", false);
                _funcBoss.SpawnEnemie(animation, minX, maxX, minY, maxY);

                break;
            case EstadoVida.Rojo:
                _funcBoss.timeToShootCont += Time.deltaTime;
                _funcBoss.ShootAtack(bala[balarandom], zonadisp);
                _funcBoss.SpawnEnemie(animation, minX, maxX, minY, maxY);
                break;
        }
    }
    public void RecibirDamage(float damage)
    {
        hp -= damage;

        if (hp>=75)
        {
            estado = EstadoVida.Verde;
        }
        else if (hp<75 && hp>= 40)
        {
            estado = EstadoVida.Amarillo;
        }
        else if (hp < 40 && hp>=1)
        {
            estado = EstadoVida.Rojo;
        }
        if (hp <= 0)
        {
            die = true;
            animation.SetTrigger("TriggerDeath");
            
            // Destroy(this.gameObject, 1f);
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(!die){
            if (hp > 5)
            {
                animation.SetTrigger("RecibirDaņo");
            }


            if (collision.tag == "Arrow") RecibirDamage(collision.gameObject.GetComponent<Arrow_Controler>().damage);
        }

    }
    public void OnDeathDisable()
    {
        gameObject.SetActive(false);
        diescore = GameManager.Instance.actualscore;
    }
}