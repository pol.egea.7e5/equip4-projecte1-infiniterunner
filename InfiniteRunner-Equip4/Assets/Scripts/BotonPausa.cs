using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BotonPausa : MonoBehaviour
{
    public GameObject panel;

   public void Pausa()
    {
        Time.timeScale = 0f;
        panel.SetActive(true);
    }
    public void Reanudar()
    {
        Time.timeScale = 1f;
        panel.SetActive(false);
    }

    public void Menu()
    {
        SceneManager.LoadScene("StartScene");
    }

}
